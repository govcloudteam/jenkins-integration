# Complete your provider information here
# Full list of providers: https://www.terraform.io/docs/providers/index.html
provider "aws" {
    # provider parameters here. Override any secrets at run time and avoid storing them in source control
    region = var.aws_region    
}

# Make a single call to the component module in the modules folder of this repo.
# Do not create any resources nor reference other modules here.
# That belongs in ../../../modules/compute/compute.tf
module "compute" {
    source = "../../../modules/compute"
    # other variables to be passed in go here   
    ami = var.ami
    instance_count = var.instance_count
    instance_tags = var.instance_tags
    instance_type = var.instance_type
    aws_region =  var.aws_region
    aws_dynamodb_table = var.aws_dynamodb_table
}

terraform {
    backend "s3" {
        bucket = "cj-terraform-remote-state"
        key    = "terraservices/dev/compute/terraform.tfstate"
        region = "us-east-1"
    }
}

resource "random_id" "dynamo-tf-state" {
  byte_length = 2
}
resource "aws_dynamodb_table" "terraform_dev_statelock" {
  name = "${var.aws_dynamodb_table}-${random_id.dynamo-tf-state.dec}"
  read_capacity = 20
  write_capacity = 20
  hash_key = "LockID"

  attribute {
      name = "LockID"
      type = "S"
  }
}

