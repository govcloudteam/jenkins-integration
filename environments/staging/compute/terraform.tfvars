# Supply values for your environment-specific variables here
ami = "ami-0e9089763828757e1"
instance_count = "1"
instance_tags = "my.ec2"
instance_type = "t2.micro"
aws_region = "us-east-1"
aws_dynamodb_table = "tf-remote-staging-state-lock"
