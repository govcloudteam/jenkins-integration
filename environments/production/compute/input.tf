# variables declarations that are passed in to the component module go here
# supply the actual values in terraform.tfvars within the environment folder
variable "ami" { }
variable "instance_count" { }
variable "instance_tags" { }
variable "instance_type" { }
variable "aws_region" { }
variable "aws_dynamodb_table" { }


