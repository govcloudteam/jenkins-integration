resource "random_id" "ec2-name" {
  byte_length = 2
}
resource "aws_instance" "cj-tf-demo-ec2" {
  count         = var.instance_count
  ami           = var.ami
  instance_type = var.instance_type
  
  tags = {
    Name = "${var.instance_tags}-${random_id.ec2-name.dec}"
  }
}
